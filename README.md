# Idris2 Quickdocs

This is an index generator and fast documentation browser for
[Idris2](https://github.com/idris-lang/Idris2).

You can view it at https://idris2-quickdocs.surge.sh/ .

## How to update search indices
```
./populate_data
```

